## Config API
_Keywords: JSON, Config, Comments_

This API introduces Annotations to define a config file and comments. The de-/serializations is done by [gson](https://github.com/google/gson).
Using [Lombok](https://projectlombok.org/) is advised. Already loaded config files will be cached.

### Example TestConfig.java
The java class file for the config:
```java
@Data
@NoArgsConstructor
@Config("test.json")
public class TestConfig
{

    @Comment("A Name")
    private String name = "A special name";

    @Comment({
            "An integer value",
            "With another config line"
    })
    private int someIntValue = 1234;

    @Comment("A dummy auth entry")
    private DummyAuthenticationEntry authenticationEntry = new DummyAuthenticationEntry();

    @Comment("A UUID List")
    private List<UUID> uuids = new ArrayList<UUID>()
    {{
        add( UUID.randomUUID() );
        add( UUID.randomUUID() );
    }};

}
```

How to load and use the config:

Use the ConfigStore class with a directory path where the config files should be saved. Then simply use the save, load or reload methods.
```java
final String executionPath = System.getProperty( "user.dir" );
final ConfigStore configStore = new ConfigStore( executionPath );

//Load config from file
TestConfig config = configStore.load( TestConfig.class );

//Change config and save
config.setName("A new name");
configStore.save(config);

```

The config file will look like this:
```json
{
  //A Name
  "name": "A special name",
  //An integer value
  //With another config line
  "someIntValue": 1234,
  //A dummy auth entry
  "authenticationEntry": {
    "user": "user",
    //the password needs to be changed!
    "password": "password",
    "securityRealm": "SuperSecretServer"
  },
  //A UUID List
  "uuids": [
    "f4d702dc-31f3-4da4-85e9-ebae3059fded",
    "322cda05-6c1a-4ae4-acc5-1a9e0e475df4"
  ]
}
```