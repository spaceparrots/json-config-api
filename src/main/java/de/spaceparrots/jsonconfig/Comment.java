package de.spaceparrots.jsonconfig;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <h1>Comment</h1>
 * The Comment annotation adds comments
 * into a json typed config file
 *
 * @author Kevin Mattutat
 * @version 19.12.2018
 * @since 19.12.2018
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Comment
{

    /**
     * The comment lines.
     *
     * @return array of comment lines
     */
    String[] value();
}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/