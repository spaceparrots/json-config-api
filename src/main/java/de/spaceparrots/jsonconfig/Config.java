package de.spaceparrots.jsonconfig;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * <h1>Config</h1>
 * The Config annotation marking a class
 * to be parsable into a json typed config file
 *
 * @author Kevin Mattutat
 * @version 19.12.2018
 * @since 19.12.2018
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Config
{

    /**
     * Filename string.
     *
     * @return the filename string
     */
    String value() default "config";
}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/