package de.spaceparrots.jsonconfig;

import com.google.gson.FieldNamingStrategy;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.lang.reflect.Field;
import java.util.LinkedHashMap;

/**
 * <h1>ConfigFieldNamingStrategy</h1>
 * The ConfigFieldNamingStrategy tricks the {@link com.google.gson.Gson} parser by using the
 * {@link FieldNamingStrategy} interface. It translates a field to a placeholder strings so that it
 * can be replaced with a commented field later.
 *
 * @author Kevin Mattutat
 * @version 19.12.2018
 * @since 19.12.2018
 */
@Getter
final class ConfigFieldNamingStrategy implements FieldNamingStrategy
{

    private FieldNamingStrategy fallback;

    private LinkedHashMap<String, PlaceHolderContext> placeholderMap;

    /**
     * Instantiates a new ConfigFieldNamingStrategy.<br>
     * Fields without {@link Comment} annoations are translated
     * by the {@link ConfigFieldNamingStrategy#fallback}
     *
     * @param fallback the fallback FieldNamingStrategy for the default field name
     */
    public ConfigFieldNamingStrategy( FieldNamingStrategy fallback )
    {
        this.fallback = fallback;
        this.placeholderMap = new LinkedHashMap<>();
    }

    @Override
    public String translateName( Field field )
    {
        Comment comment = field.getAnnotation( Comment.class );
        String fieldName = fallback.translateName( field );

        if ( comment != null )
        {
            String placeholder = String.format( "comment_placeholder_%s", this.placeholderMap.size() + 1 );
            this.placeholderMap.put( placeholder, new PlaceHolderContext(
                    comment.value(),    //Comment text
                    fieldName           //the original field name
            ) );
            return placeholder;
        }
        return fieldName;
    }

    /**
     * <h1>PlaceHolderContext</h1>
     * The PlaceHolderContext helper class to
     * store the comments and original field names.
     */
    @Getter
    @AllArgsConstructor
    @ToString
    @EqualsAndHashCode
    public class PlaceHolderContext
    {

        private String[] comment;
        private String field;
    }
}
