package de.spaceparrots.jsonconfig;

import com.google.gson.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

/**
 * <h1>ConfigStore</h1>
 * The ConfigStore can load objects marked with the {@link Comment} annotation
 * from the the data folder containing the corresponding files.
 *
 * @author Kevin Mattutat
 * @version 19.12.2018
 * @since 19.12.2018
 */
public final class ConfigStore
{

    private File dataFolder;
    private FieldNamingStrategy namingStrategy;
    private ExclusionStrategy exclusionStrategy;

    private HashMap<Class<?>, Object> configCache;

    /**
     * Instantiates a ConfigStore.
     *
     * @param dataFolder the data folder containing config files
     */
    public ConfigStore( String dataFolder )
    {
        this( dataFolder, FieldNamingPolicy.IDENTITY );
    }

    /**
     * Instantiates a ConfigStore.
     * Use the {@link FieldNamingPolicy} enum to the the <code>fieldNamingStrategy</code>.
     *
     * @param dataFolder          the data folder containing config files
     * @param fieldNamingStrategy the field naming strategy
     */
    public ConfigStore( String dataFolder, FieldNamingStrategy fieldNamingStrategy )
    {
        this.dataFolder = new File( dataFolder );
        this.namingStrategy = fieldNamingStrategy;
        this.exclusionStrategy = new FieldExclusionStrategy();
        this.configCache = new HashMap<>();
    }


    /**
     * Loads the config from a file to initialize a object
     * class extending the {@link Config} annotation. The next call for this
     * target class, will load the config object from the cache.
     *
     * @param <T>  the type parameter
     * @param type the type
     *
     * @return the loaded or cached config object
     *
     * @throws InvalidConfigException the invalid config exception
     */
    public <T> T load( Class<T> type ) throws InvalidConfigException
    {
        try
        {
            return (T) this.configCache.getOrDefault( type, readFromFile( type ) );
        }
        catch ( FileNotFoundException e )
        {
            try
            {
                //File is not initialized yet -> write initial config
                Constructor<T> constructor = type.getDeclaredConstructor();
                T configInstance = constructor.newInstance();
                save( configInstance );
                return configInstance;
            }
            catch ( NoSuchMethodException e1 )
            {
                throw new InvalidConfigException( type, "A config class requires a public default constructor for the initial config file!" );
            }
            catch ( IllegalAccessException | InstantiationException | InvocationTargetException e1 )
            {
                throw new InvalidConfigException( type, "The config could not be initialized. It requires a public default constructor!" );
            }

        }
    }

    /**
     * Loads the config from a file to initialize a object
     * class extending the {@link Config} annotation. The next call for this
     * target class, will load the config object from the cache.
     *
     * @param <T>  the type parameter
     * @param type the type
     *
     * @return the loaded or cached config object
     *
     * @throws InvalidConfigException the invalid config exception
     */
    public <T> T load( Class<T> type, File file ) throws InvalidConfigException
    {
        try
        {
            return readFromFile( type, file );
        }
        catch ( FileNotFoundException e )
        {
            try
            {
                //File is not initialized yet -> write initial config
                Constructor<T> constructor = type.getDeclaredConstructor();
                T configInstance = constructor.newInstance();
                save( configInstance );
                return configInstance;
            }
            catch ( NoSuchMethodException e1 )
            {
                throw new InvalidConfigException( type, "A config class requires a public default constructor for the initial config file!" );
            }
            catch ( IllegalAccessException | InstantiationException | InvocationTargetException e1 )
            {
                throw new InvalidConfigException( type, "The config could not be initialized. It requires a public default constructor!" );
            }

        }
    }

    /**
     * Reload the config from a file to initialize a object
     * class extending the {@link Config} annotation.
     *
     * @param <T>  the type parameter
     * @param type the type
     *
     * @return the loaded config object
     *
     * @throws InvalidConfigException the invalid config exception
     */
    public <T> T reload( Class<T> type ) throws InvalidConfigException
    {
        try
        {
            return readFromFile( type );
        }
        catch ( FileNotFoundException e )
        {
            throw new InvalidConfigException( type, "File not found!" );
        }
    }


    /**
     * Saves a object of a class extending the {@link Config} annotation.
     *
     * @param <T>    the type
     * @param config the config object
     *
     * @throws InvalidConfigException the invalid config exception
     */
    public <T> void save( T config ) throws InvalidConfigException
    {
        File configFile = tryOpenFile( config.getClass() );
        save( config, configFile );
    }

    /**
     * Saves a object of a class extending the {@link Config} annotation.
     *
     * @param <T>        the type
     * @param config     the config object
     * @param configFile the config file
     *
     * @throws InvalidConfigException the invalid config exception
     */
    public <T> void save( T config, File configFile )
    {
        ConfigFieldNamingStrategy configFieldNamingStrategy = new ConfigFieldNamingStrategy( this.namingStrategy );
        Gson gson = new GsonBuilder()
                .setPrettyPrinting() //Print per line
                .setLenient() //Ignore comments, etc.
                .setFieldNamingStrategy( configFieldNamingStrategy ) //Adds placeholder for comments to field names
                .setExclusionStrategies( this.exclusionStrategy ) //Exclude everything with exclude annotations
                .create();
        try
        {
            String json = gson.toJson( config );

            //Preprocess json
            for ( Map.Entry<String, ConfigFieldNamingStrategy.PlaceHolderContext> entry :
                    configFieldNamingStrategy.getPlaceholderMap().entrySet() )
            {
                int JSON_LENGHT = json.length();

                String placeholder = entry.getKey();
                ConfigFieldNamingStrategy.PlaceHolderContext context = entry.getValue();

                //Count whitespaces for indentation
                int spaces = JSON_LENGHT - json.replaceFirst( ".+\"" + placeholder + "\"", "" ).length() - placeholder.length() - 2;

                String indent = indent( spaces );

                json = json.replaceFirst( "\"" + placeholder + "\"",
                        String.format( "//%s%n%s\"%s\"", prepareComment( indent, context.getComment() ), indent, context.getField() ) );
            }

            try ( PrintWriter out = new PrintWriter( configFile ) )
            {
                out.println( json );
            }
        }
        catch ( Exception e )
        {
            e.printStackTrace();
        }
    }

    public void clearCache()
    {
        this.configCache.clear();
    }

    private String indent( int spaces )
    {
        return new String( new char[spaces] ).replace( "\0", " " );
    }

    private String prepareComment( String indent, String... comments )
    {
        return String.join( String.format( "%n%s//", indent ), comments );
    }

    private <T> T readFromFile( Class<T> type, File configFile ) throws FileNotFoundException
    {
        Gson gson = new GsonBuilder()
                .setLenient() //Ignore comments, etc.
                .setFieldNamingStrategy( this.namingStrategy )
                .create();

        FileReader reader = new FileReader( configFile );
        T configObj = gson.fromJson( reader, type );
        configCache.put( type, configObj );
        return configObj;

    }

    private <T> T readFromFile( Class<T> type ) throws InvalidConfigException, FileNotFoundException
    {
        File configFile = tryOpenFile( type );

        Gson gson = new GsonBuilder()
                .setLenient() //Ignore comments, etc.
                .setFieldNamingStrategy( this.namingStrategy )
                .create();

        FileReader reader = new FileReader( configFile );
        T configObj = gson.fromJson( reader, type );
        configCache.put( type, configObj );
        return configObj;

    }

    private File tryOpenFile( Class configClass ) throws InvalidConfigException
    {
        Config annotation = (Config) configClass.getAnnotation( Config.class );

        //Check for config annotation
        if ( annotation == null )
            throw new InvalidConfigException( configClass, "Missing Config annotation" );

        //Load data folder
        if ( ( !this.dataFolder.exists() && !this.dataFolder.mkdir() ) || !this.dataFolder.isDirectory() )
            throw new InvalidConfigException( configClass, "Invalid directory: " + this.dataFolder.getAbsolutePath() );

        //Create file instance
        return new File( dataFolder, annotation.value() );

    }


}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/