package de.spaceparrots.jsonconfig;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;

/**
 * <h1>ExclusionStrategy</h1>
 * The FieldExclusionStrategy uses the {@link ExclusionStrategy} interface to
 * watch for the {@link Exclude} annotation on field to ignore them.
 *
 * @author Kevin Mattutat
 * @version 19.12.2018
 * @since 19.12.2018
 */
final class FieldExclusionStrategy implements ExclusionStrategy
{

    @Override
    public boolean shouldSkipClass( Class<?> clazz )
    {
        return false;
    }

    @Override
    public boolean shouldSkipField( FieldAttributes field )
    {
        return field.getAnnotation( Exclude.class ) != null;
    }

}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/