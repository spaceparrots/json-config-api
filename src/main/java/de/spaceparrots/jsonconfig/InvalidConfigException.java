package de.spaceparrots.jsonconfig;

/**
 * <h1>InvalidConfigException</h1>
 * InvalidConfigException is thrown while using {@link ConfigStore} load, reload or save.
 *
 * @author Kevin Mattutat
 * @version 19.12.2018
 * @since 19.12.2018
 */
public final class InvalidConfigException extends Exception
{

    /**
     * Instantiates a InvalidConfigException.
     *
     * @param configclass the config class
     */
    public InvalidConfigException( Class configclass )
    {
        super( configclass.getTypeName() );
    }

    /**
     * Instantiates a InvalidConfigException.
     *
     * @param configclass the config class
     * @param message     the message
     */
    public InvalidConfigException( Class configclass, String message )
    {
        super( String.format( "(%s): ", configclass.getTypeName(), message ) );
    }

    /**
     * Instantiates a InvalidConfigException.
     *
     * @param message the message
     */
    public InvalidConfigException( String message )
    {
        super( message );
    }
}
/***********************************************************************************************
 *
 *                  All rights reserved, SpaceParrots UG (c) copyright 2018
 *
 ***********************************************************************************************/